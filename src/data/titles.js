export const SITE_NAME = 'The Racing Manager'

export const PAGE_NOT_FOUND = 'Page not found'
export const HOMEPAGE = 'Homepage'
export const REGISTER = 'Register'
export const REGISTRATION_SUCCESSFUL = 'Registration successful'
export const BROWSE_HORSES = 'Browse horses'
export const MEMBER_DASHBOARD = "Member's dashboard"
export const ACCOUNT = 'Account'