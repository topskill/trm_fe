export const LOGGED_OUT = 'Successfully logged out'

export const userLoggedInMessage = (name = '') => {
  return `Welcome back ${name}`
}

export const UPDATED_USER_DETAILS = 'Successfully updated details'
